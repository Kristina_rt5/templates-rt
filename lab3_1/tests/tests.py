import pytest

from tests.testProcess import read_line, write_line, get_process, read_lines

program_under_test = './../cmake-build-debug/lab3_1'
program_args = []
second = 1
action_timeout = 0.5 * second


async def write_lines(input_str: str) -> [str]:
    process = await get_process(program_under_test, *program_args)
    write_line(process.stdin, input_str.encode("utf-8"))
    byte_lines = await read_lines(process.stdout, action_timeout)
    decoded_lines = [x.decode("utf-8") for x in byte_lines]
    result = "".join(decoded_lines)
    return result


@pytest.mark.asyncio
async def test_1():
    expected_output = "Введите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n0\nПродолжить работу? (y/n)\n"
    input_str = "4\n1\nn\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_2():
    expected_output = "Введите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n35\nПродолжить работу? (y/n)\nВведите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n45\nПродолжить работу? (y/n)\n"
    input_str = "23\n3\ny\n34\n2\nn\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_3():
    expected_output = "Введите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n35\nПродолжить работу? (y/n)\nВведите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n45\nПродолжить работу? (y/n)\n"
    input_str = "23\n3\ny\n34\n2\nn\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output

@pytest.mark.asyncio
async def test_4():
    expected_output = "Введите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n15\nПродолжить работу? (y/n)\nВведите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n50\nПродолжить работу? (y/n)\n"
    input_str = "12\n12\ny\n24\n6\nn\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output


@pytest.mark.asyncio
async def test_5():
    expected_output = "Введите количество натуральных чисел n:\nВведите значение m:\nРезультат:\n43565\nПродолжить работу? (y/n)\n"
    input_str = "666\n66\nn\n"
    received_output = await write_lines(input_str)
    assert received_output == expected_output