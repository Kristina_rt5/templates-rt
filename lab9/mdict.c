#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define MAX_WORD_LENGTH 30
#define MAX_DICT_LENGTH 100


// это ключ-значение
struct translate {
    // Слово на английском
    char en[MAX_WORD_LENGTH];
    // Слово на русском
    char ru[MAX_WORD_LENGTH];
};

struct Dictionary {
    struct translate mas[MAX_DICT_LENGTH];
};

char menu() {
    char n[10];
    scanf("%s", &n);
    return n[0];
}

//проверка на уже существующее слово в словаре
void add_w(struct Dictionary *dict, int num_w,char control) {
    struct translate word;
    memset(word.en, 0, MAX_WORD_LENGTH);
    memset(word.ru, 0, MAX_WORD_LENGTH);
    if(control=='1'){
        printf("Введите слово на английском: \n");
        scanf("%s", &word.en);
        printf("Введите слово на русском: \n");
        scanf("%s", &word.ru);
    }
    else{
        printf("Введите слово на английском: \n");
        strcpy(word.en,"potato");
        printf("Введите слово на русском: \n");
        strcpy(word.ru,"kartoshka");
        printf("Введенное слово: potato - kartoshka \n");
    }
    dict->mas[num_w + 1] = word;
}

void swap(char *a,char *b){
    char tmp[MAX_WORD_LENGTH];
    strncpy(tmp,a, MAX_WORD_LENGTH);
    strncpy(a,b, MAX_WORD_LENGTH);
    strncpy(b,tmp, MAX_WORD_LENGTH);
}

/*
Положительное число – если строки отличаются и код первого отличающегося символа в строке str1 больше кода символа на той же позиции в строке str2.
 2 1
Отрицательное число – если строки отличаются и код первого отличающегося символа в строке str1 меньше кода символа на той же позиции в строке str2.
 1 2
 */
void sort(struct Dictionary *dict,int num_w){
    //struct translate word;

    for(int i=0;i<num_w-1;i++){
        for(int j=0;j<num_w-1-i;j++){
            char *word1=dict->mas[j].en;
            char *word2=dict->mas[j+1].en;

            if(strcmp(word1,word2)>0){
                swap(word1,word2);
                swap(dict->mas[j].ru,dict->mas[j+1].ru);
            }
        }
    }
}



int str_cmp(const char *a, const char *b){
    while(*a){
        if(*a!=*b){
            break;
        }
        a++;
        b++;
    }
    return *(const unsigned char*)a -*(const unsigned char*)b;
}

int find(struct Dictionary dict,char control) {
    char a[MAX_WORD_LENGTH];
    //memset(a, 0, MAX_WORD_LENGTH);
    for(int i=0;i<MAX_WORD_LENGTH;i++) a[i]='0';
    if(control=='1'){
        scanf("%30s", &a);
    }
    else{
        strcpy(a,"raduga");
        printf("Введенное слово: raduga \n");
    }
    for (int i = 0; i < MAX_DICT_LENGTH; i++) {
        if (str_cmp(dict.mas[i].en, a)==0 || str_cmp(dict.mas[i].ru, a)==0) {
            return i;
        }
    }
    return -1;
}

/*
Положительное число – если строки отличаются и код первого отличающегося символа в строке str1 больше кода символа на той же позиции в строке str2.
 2 1
Отрицательное число – если строки отличаются и код первого отличающегося символа в строке str1 меньше кода символа на той же позиции в строке str2.
 1 2
 */
int binary_find(struct Dictionary dict,int num_w,char control){
    char a[MAX_WORD_LENGTH];
    for(int i=0;i<MAX_WORD_LENGTH;i++) a[i]='0';
    int k=num_w/2;
    char *word1=dict.mas[k].en;
    if(control=='1'){
        scanf("%30s", &a);
    }
    else{
        strcpy(a,"big");
        printf("Введенное слово: big \n");
    }

    for(int i=0;(int)log2((double)num_w)+1>i;i++){
        if (str_cmp(a,word1)==0){
            return k;
        }

        if (str_cmp(a,word1)<0){
            k=k/2;
            word1=dict.mas[k].en;
        }
        else{
            k=(num_w+k)/2;
            word1=dict.mas[k].en;
        }
    }
    return -1;
}



/*
 * int aa=sizeof(a);
    int bb=sizeof(b);
    if(aa<bb){
        bb=aa;
    }
    while(a){
        for(int i=0;i<bb;)
        if(a!=b){
            break;
        }

    }
 */


void del_w(struct Dictionary *dict, int ind) {
    if (ind == -1) {
        printf("Такого слова нет\n");
    } else {
        for (; ind < MAX_DICT_LENGTH - 1; ind++) {
            dict->mas[ind] = dict->mas[ind + 1];
        }
        dict->mas[MAX_DICT_LENGTH - 1] = (struct translate) {"", ""};
    }
}

void print_dict(struct Dictionary dict, int num_w) {
    for (int i = 0; i <= num_w; i++) {
        printf("%s - %s\n", dict.mas[i].en, dict.mas[i].ru);
    }
}

void translate_en_ru(struct Dictionary dict, int ind) {
    if (ind == -1) {
        printf("Такого слова нет\n");
    }else {
        printf("%s\n", dict.mas[ind].ru);
    }
}

void translate_ru_en(struct Dictionary dict, int ind) {
    if (ind == -1) {
        printf("Такого слова нет\n");
    } else {
        printf("%s\n", dict.mas[ind].en);
    }
}

void print_in_file(struct Dictionary dict, int num_w){
    FILE *fp;
    char name[] = "Dictionary.txt";
    if ((fp = fopen(name, "w")) == NULL){
        printf("Не удалось открыть файл");
    }
    for (int i = 0; i <= num_w; i++) {
        fprintf(fp,"%s - %s\n", dict.mas[i].en, dict.mas[i].ru);
    }
    fclose(fp);
}


int interactive() {
    //setlocale(LC_ALL,"Russian");
    system("chcp 65001");
    struct Dictionary dict;
    struct translate basic_translations[] = {
            {"cat",      "kot"},
            {"dog",      "sobaka"},
            {"big",      "bolshoy"},
            {"sun",      "solnce"},
            {"bird",     "ptica"},
            {"rainbow",  "raduga"},
            {"BMSTU",    "MGTU"},
            {"girl",     "devochka"},
            {"boy",      "malchik"},
            {"Kristina", "solnyshko"}
    };
    for (int i = 0; i < sizeof(basic_translations) / sizeof(struct translate); i++) dict.mas[i] = basic_translations[i];
    int num_w = 9;
    printf("1)\tдобавление слов в словарь;\n"
           "2)\tудаление слов из словаря; \n"
           "3)\tперевод слов с английского на русский;\n"
           "4)\tперевод слов с русского на английский;\n"
           "5)\tпросмотр словаря (вывод на экран словаря из ОП);\n"
           "6)\tвывод словаря в файл;\n"
           "7)\tвыход.\n\nВведите число от 1 до 7:\t");
    sort(&dict,num_w);
    while (1) {
        switch (menu()) {
            case '1':
                add_w(&dict, num_w,'1');
                num_w++;
                sort(&dict,num_w);
                break;
            case '2':
                printf("\nВведите слово, которое нужно удалить: \n");
                del_w(&dict, find(dict,'1'));
                num_w--;
                break;
            case '3':
                printf("\nВведите слово на английском:\n");
                translate_en_ru(dict, binary_find(dict,num_w,'1'));
                break;
            case '4':
                printf("\nВведите слово на русском:\n");
                translate_ru_en(dict, find(dict,'1'));
                break;
            case '5':
                print_dict(dict, num_w);
                break;
            case '6':
                printf("\nЗапись словаря в файл\n");
                print_in_file(dict,num_w);
                break;
            case '7':
                return 0;
            default:
                printf("\nОшибка. Введите число от 1 до 7: ");
        }
    }
}



int demo(){
    system("chcp 65001");
    struct Dictionary dict;
    struct translate basic_translations[] = {
            {"cat",      "kot"},
            {"dog",      "sobaka"},
            {"big",      "bolshoy"},
            {"sun",      "solnce"},
            {"bird",     "ptica"},
            {"rainbow",  "raduga"},
            {"BMSTU",    "MGTU"},
            {"girl",     "devochka"},
            {"boy",      "malchik"},
            {"sun", "solnyshko"}
    };
    for (int i = 0; i < sizeof(basic_translations) / sizeof(struct translate); i++) dict.mas[i] = basic_translations[i];
    int num_w = 9;
    printf("1)\tдобавление слов в словарь;\n"
           "2)\tудаление слов из словаря; \n"
           "3)\tперевод слов с английского на русский;\n"
           "4)\tперевод слов с русского на английский;\n"
           "5)\tпросмотр словаря (вывод на экран словаря из ОП);\n"
           "6)\tвывод словаря в файл;\n"
           "7)\tвыход.\n\nВведите число от 1 до 7:\t");
    sort(&dict,num_w);
    while (1) {
        char d[]="h1234567";
        for(int i=0;i<8;i++){
            switch (d[i]) {
                case '1':
                    add_w(&dict, num_w,'0');
                    num_w++;
                    sort(&dict,num_w);
                    break;
                case '2':
                    printf("\nВведите слово, которое нужно удалить: \n");
                    del_w(&dict, find(dict,'0'));
                    num_w--;
                    break;
                case '3':
                    printf("\nВведите слово на английском:\n");
                    translate_en_ru(dict, binary_find(dict,num_w,'0'));
                    break;
                case '4':
                    printf("\nВведите слово на русском:\n");
                    translate_ru_en(dict, find(dict,'0'));
                    break;
                case '5':
                    printf("\n");
                    print_dict(dict, num_w);
                    break;
                case '6':
                    printf("\nЗапись словаря в файл\n");
                    print_in_file(dict,num_w);
                    break;
                case '7':
                    return 0;
                default:
                    printf("\nОшибка. Введите число от 1 до 7: ");
            }
        }
    }

}
