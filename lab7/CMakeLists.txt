cmake_minimum_required(VERSION 3.23)
project(lab7 C)

set(CMAKE_C_STANDARD 17)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/source.txt
        DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/key.txt
        DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

add_executable(lab7 main.c caesar.c)

IF (NOT WIN32)
    target_link_libraries(lab7 m)
ENDIF ()
